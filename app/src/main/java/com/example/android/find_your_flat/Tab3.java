package com.example.android.find_your_flat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.florescu.android.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener;

/**
 * Created by andronov on 21-May-17.
 */

public class Tab3 extends android.support.v4.app.Fragment {

    private SearchCriteria criteria;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View inflatedView = inflater.inflate(R.layout.tab3, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            criteria = bundle.getParcelable("searchCriteria");
            Log.i("Angelina logs: ", "parcelable not null, is buy = "+ criteria.isBuy());
        }

        //initialize seekbar and textfield
        RangeSeekBar seekBar = (RangeSeekBar) inflatedView.findViewById(R.id.rangeSeekBar);
        final TextView minText = (TextView) inflatedView.findViewById(R.id.minValueText);
        final TextView maxText = (TextView) inflatedView.findViewById(R.id.maxValueText);


        if(criteria !=null ) {
            minText.setText(formatObject(criteria.getMinCost()));
            maxText.setText(formatObject(criteria.getMaxCost()));
        }

        seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                minText.setText(formatObject(minValue));
                if(criteria !=null ) {
                    criteria.setMinCost((int) minValue);
                    Log.i("Angelina logs: ", "parcelable not null, minValue = "+ criteria.getMinCost());
                }

                maxText.setText(formatObject(maxValue));
                if(criteria !=null ) {
                    criteria.setMaxCost((int) maxValue);
                    Log.i("Angelina logs: ", "parcelable not null, maxValue = "+ criteria.getMaxCost());
                }

            }

        });




        return inflatedView;

    }
    String formatObject(Object o){
        return String.format("%,d", o).replace(",", " ");
    }

}






