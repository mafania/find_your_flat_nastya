package com.example.android.find_your_flat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Nastya on 29-May-17.
 */


/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class Tab2 extends Fragment implements OnMapReadyCallback {
    FragmentTransaction transaction;
    public static final String TAG=Tab2.class.getSimpleName();

    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;
    public Tab2(){
        //required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");

        if (mView != null) {
            ViewGroup viewGroupParent = (ViewGroup) mView.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(mView);
        } else {
            try {
                mView = inflater.inflate(R.layout.tab2, container, false);
                Log.i(TAG, "onCreateView: tab2 fragment inflated");
            } catch (Exception e) {
                Log.i(TAG, "exception mView inflation" + e.getMessage());
            }
        }

        Button button_center = (Button) mView.findViewById(R.id.button_center);

        button_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(),"Click Me", Toast.LENGTH_LONG);
                toast.show();
                SimpleOrientationActivity myOrientationEventListener = new SimpleOrientationActivity();
            }
        });

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        Log.i(TAG, "onViewCreated");
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        transaction = getChildFragmentManager().beginTransaction();
        if(fragment == null){
//            FragmentManager manager = getFragmentManager();
//            fragment =SupportMapFragment.newInstance();
//            manager.beginTransaction().replace(R.id.map, fragment).commit();

            //Log.d(TAG, "onViewCreated: fragment doesn't exist");
            SupportMapFragment newFragment= new SupportMapFragment();
            transaction.add(R.id.map, newFragment);

        }
        else {
            //fragment.getMapAsync(this);
            Log.d(TAG, "onViewCreated: map fragment created");
            transaction.replace(R.id.map, fragment);
        }
        transaction.commit();
    }
    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "onMapReady");
        //Callback interface for when the map is ready to be used.

        MapsInitializer.initialize(getContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(48.689247, -74.044582))
                    .title("Statue of Liberty")
                    .snippet("I went there already!"));
        CameraPosition Liberty = CameraPosition
                .builder()
                .target(new LatLng(48.689247, -74.044582))
                .zoom(16)
                .bearing(8)
                .tilt(45)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Liberty));

    }
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "onDestroyView");
    }
}


