CREATE DATABASE  IF NOT EXISTS `find_your_flat 
USE find_your_flat;


--
-- Table structure for table `property_type`
--

DROP TABLE IF EXISTS property_type;

CREATE TABLE property_type (
  id int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  description varchar(100) NULL,
  PRIMARY KEY (id)
);


--
-- Dumping data for table `property_type`
--

INSERT INTO property_type VALUES (1,'1+kk',NULL),(2,'1+1',NULL),(3,'2+kk',NULL),(4,'2+1',NULL),(5,'3+kk',NULL),(6,'3+1',NULL),(7,'4+kk',NULL),(8,'4+1',NULL),(9,'5+kk',NULL),(10,'5+1',NULL),(11,'6 or more',NULL),(12,'Atypical',NULL);


--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS property;

CREATE TABLE property (
  id int(11) NOT NULL,
  is_flat char(1) NOT NULL,
  is_buy char(1) NOT NULL,
  property_type_id int(11) NOT NULL,
  size_sqm decimal(10,2) NULL,
  location_id int(11) NOT NULL,
  owner_id int(11) NOT NULL,
  description varchar(200) NULL,
  PRIMARY KEY (id),
  KEY fk_property_type_idx (property_type_id),
  CONSTRAINT fk_property_type FOREIGN KEY (property_type_id) REFERENCES property_type (id)
);


--
-- Dumping data for table `property`
--

INSERT INTO property VALUES (2,'Y','N',10,93.50,1,1,NULL);


