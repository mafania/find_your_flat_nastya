# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This is the repository of Czechitas Coding Club Android project dedicated to creation of a mobile app, which will allow users to search for flat/house in Prague.

* Version

The team is working on the first release which will involve 4 tabbed screens used to user input and display of search results.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Organization ###

* Scrum dashboard on Trello: https://trello.com/b/t3LTe8Vs/find-your-home
* Meetings: 2x month, Thursdays at 6.30pm at the 6th floor of MSD Building.


### Contribution guidelines ###



### Who do I talk to? ###

* Anna Pascenko - Czechitas mentor and professional Java developer
* Angelina Andronova - Developer and repository admin
* Lenka Cardova - Product Owner and the ideas generator
* Deana Bielick� - Android Developer and the one who makes it beautiful
* Karolina Brizkova - mystery team member who still needs to be uncovered